import logging
import uuid
from odoo import http
from odoo.exceptions import Warning, AccessDenied
from werkzeug.exceptions import Forbidden, NotAcceptable

logger = logging.getLogger(__name__)


class AmanitaBaseController(http.Controller):

    @http.route('/amanita_base/console/auth', auth='public')
    def check_auth(self, token):
      if http.request.env['amanita_base.cache'].get(token,
                                                    family='console_auth'):
        return 'ok'
      else:
        return 'failed'


    @http.route('/amanita_base/console', auth='user')
    def spawn_terminal(self, system_name):
      console_uri = http.request.env[
              'res.config.settings'].get_amanita_param('console_uri')
      # Put auth token in cache
      user = http.request.env['res.users'].browse(http.request.env.uid)
      if user.has_group('amanita_base.group_amanita_base_admin'):
        auth_token = uuid.uuid4().hex
        # Put the token for short period in cache so that terminado server can check it.
        http.request.env['amanita_base.cache'].put(
                          auth_token, '1', family='console_auth', expire=90)
      else:
        auth_token = '404'
      page = """
          <!doctype html>
          <html>
            <head>
              <link rel="stylesheet" href="/amanita_base/static/lib/xterm/dist/xterm.css" />
              <script src="/amanita_base/static/lib/xterm/dist/xterm.js"></script>      
              <script src="/amanita_base/static/lib/xterm/dist/addons/terminado/terminado.js"></script>      
              <script src="/amanita_base/static/lib/xterm/dist/addons/fit/fit.js"></script>      
            </head>
            <body>
              <div id="terminal"></div>
              <script>        
                terminado.apply(Terminal);
                fit.apply(Terminal);        
                var term = new Terminal({
                  cols: 120,
                  rows: 48,                
                  convertEol: true,
                  fontFamily: `'Fira Mono', monospace`,
                  fontSize: 12,
                  rendererType: 'dom', // default is canvas
                });
                term.setOption('theme', { background: '#000000',  foreground: '#ffffff'})
                var sock = new WebSocket('%s');
                sock.addEventListener('open', function () {
                  term.terminadoAttach(sock);
                });
                term.open(document.getElementById('terminal'), focus=true);
                term.fit()
              </script>
            </body>
          </html>
        """ % '{}?system_name={}&auth={}'.format(console_uri, system_name, auth_token)
      return http.Response(page)
