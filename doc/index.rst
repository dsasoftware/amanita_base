===============================================
 Amanita Base Application documentation
===============================================

**DOCUMENTATION IS UNDER CONSTRUCTION**

.. contents::
   :depth: 4


Installation videos
-------------------

* `How to connect your existing server`_

.. _How to connect your existing server: http://youtu.be/djLifLYITmE

Installation manual
-------------------

Docker based installation
#########################

In a module package amanita_base there is a *deploy* folder where Docker compose style installation is available.

This is the prefered way of installing the **complete PBX**.

You get Odoo with Asterisk connected to it. 

Do not edit docker-compose.yml. Instead create docker-compose.overide.yml and put there
your local environment values. 

See docker-compose.override.yml.dev.example and docker-compose.override.yml.production.example
for references.

Start the project:

.. code::

 docker-compose up -d

During the installation procedure new postgresql database *amanita_base* is created and 
amanita_base addon is automatically installed.

During the installation process Asterisk Agent cannot 
connect to Odoo (as it's not ready yet) and gives Odoo connect errors but you can safely ignore them
as Agent will connect to Odoo when *amanita_base* addon is completely setup.


