# -*- coding: utf-8 -*-
from datetime import datetime
import json
import logging
from nameko.standalone.rpc import ServiceRpcProxy
from nameko.exceptions import UnknownService, RpcTimeout
from odoo import api, models, fields, _
from odoo.exceptions import UserError, ValidationError
try:
    import humanize
    HUMANIZE = False  # Fix bug with translation FileNotFound.
except ImportError:
    HUMANIZE = False
from odoo.addons.amanita_base.models.settings import get_nameko_config


logger = logging.getLogger(__name__)


class AsteriskServer(models.Model):
    _name = 'amanita_base.server'
    _description = "Asterisk Server"

    name = fields.Char(required=True)
    system_name = fields.Char(required=True)
    note = fields.Text()
    conf_files = fields.One2many(comodel_name='amanita_base.conf',
                                 inverse_name='server')
    sync_date = fields.Datetime(readonly=True)
    sync_uid = fields.Many2one('res.users', readonly=True, string='Sync by')
    cli_area = fields.Text(compute='_get_cli_area')
    last_ping = fields.Datetime(readonly=True)
    last_ping_human = fields.Char(compute='_get_last_ping_human',
                                  string='Last ping')
    hostname = fields.Char()
    language = fields.Char(default='en')
    ip_security_enabled = fields.Boolean(string=_('IP security'), default=True)
    filter_ports = fields.Char(default='5060,4569',
                    help=_('Comma separated ports to filter, e.g. 5060,5061'))
    ip_ban_seconds = fields.Integer(string=_('IP ban seconds'), default=600)
    reload_info = fields.Char(compute='_get_server_info', string='Last reload')
    start_info = fields.Char(compute='_get_server_info', string='Started')
    calls_info = fields.Char(compute='_get_server_info', string='Current calls')
    ssh_login = fields.Char(string='SSH Login')
    ssh_password = fields.Char(string="SSH Password")
    ssh_public_key = fields.Text(string='SSH Public Key')

    _sql_constraints = [
        ('system_name_uniq', 'UNIQUE(system_name)',
            'This System Name is already used.'),
    ]

    def get_rpc_timeout(self, default_timeout):
        if default_timeout:
            return default_timeout
        else:
            try:
                timeout = self.env[
                    'res.config.settings'].get_amanita_param('rpc_timeout')
                return int(timeout)
            except ValueError:
                logger.error('Bad rpc_timeout value: %s', timeout)
                # Return 10 seconds by default
                return 10

    @api.model
    def get_server_proxy(self, system_name, service_suffix, timeout=None):
        timeout = self.get_rpc_timeout(timeout)
        server = self.search([('system_name', '=', system_name)])
        return server.get_proxy(service_suffix, timeout=timeout)

    @api.multi
    def get_proxy(self, service_suffix, timeout=None):
        timeout = self.get_rpc_timeout(timeout)
        self.ensure_one()
        service = '{}_{}'.format(self.system_name, service_suffix)
        config = get_nameko_config(self)
        return ServiceRpcProxy(service, config, timeout=timeout)

    @api.multi
    def _get_last_ping_human(self):
        for rec in self:
            if HUMANIZE:
                to_translate = self.env.context.get('lang', 'en_US')
                if to_translate != 'en_US':
                    humanize.i18n.activate(to_translate)
                rec.last_ping_human = humanize.naturaltime(
                    fields.Datetime.from_string(rec.last_ping))
            else:
                rec.last_ping_human = rec.last_ping

    @api.multi
    def _get_cli_area(self):
        for rec in self:
            rec.cli_area = '/amanita_base/console?system_name={}'.format(
                rec.system_name)

    @api.multi
    def apply_changes_button(self):
        self.apply_changes()

    @api.multi
    def apply_changes(self):
        self.ensure_one()
        is_uploaded = None
        for conf in self.env['amanita_base.conf'].search([
                ('server', '=', self.id),
                ('is_updated', '=', True)]):
            is_uploaded = conf.upload_conf()
            if is_uploaded:
                conf.is_updated = False
        if is_uploaded:
            self.reload_action()


    @api.multi
    def upload_all_conf(self):
        self.ensure_one()
        with self.get_proxy('fileman') as proxy:
            for rec in self.conf_files:
                rec.is_updated = False
                proxy.put_asterisk_config(rec.name, rec.content)

        self.sudo().write({'sync_date': fields.Datetime.now(),
                           'sync_uid': self.env.uid})


    @api.multi
    def download_all_conf(self):
        self.ensure_one()
        with self.get_proxy('fileman') as proxy:
            files = proxy.get_all_asterisk_configs()
            for filename, data in files.items():
                file = self.env['amanita_base.conf'].with_context(
                    conf_no_update=True).get_or_create(self.id, filename)
                file.write({
                    'content': data,
                    'sync_date': fields.Datetime.now(),
                    'sync_uid': self.env.uid,
                })
        # Update last sync
        self.sudo().write({'sync_date': fields.Datetime.now(),
                           'sync_uid': self.env.uid})
        return True


    @api.multi
    def reload_button(self):
        self.ensure_one()
        self.reload_action(notify_uid=self.env.uid)


    def reload_action(self, module=None, notify_uid=None):
        action = {'Action': 'Reload'}
        if module:
            action['Module'] = module
        with self.get_proxy('ami') as proxy:
            result = proxy.send_action(action)['headers']['Message']
            if notify_uid:
                self.notify_user(message=result,
                                          title='Asterisk reload status')
            return True


    @api.multi
    def ping_button(self):
        self.ensure_one()
        self.ping(silent=False)


    @api.multi
    def ping(self, silent=True):
        self.ensure_one()
        try:
            with self.get_proxy('ami') as proxy:
                ts = proxy.send_action({'Action': 'Ping'})['headers']['Timestamp']
                sd = datetime.fromtimestamp(int(float(ts)))
                self.notify_user(message='Server time: {}'.format(sd),
                                          title='Asterisk ping')
        except RpcTimeout:
            raise ValidationError('Server connect timeout')


    """ # TODO: Refactor pings
    @api.model
    def ping_all(self):
        for server in self.search([]):
            server.ping(silent=True)
    """

    @api.multi
    def _get_server_info(self):        
        for rec in self:            
            try:
                with self.get_proxy('ami') as proxy:
                    res = proxy.send_action({'Action': 'CoreStatus'})['headers']
                    rec.reload_info = '{} {}'.format(res['CoreReloadDate'],
                                                     res['CoreReloadTime'])
                    rec.start_info = '{} {}'.format(res['CoreStartupDate'],
                                                     res['CoreStartupTime'])
                    rec.calls_info = res['CoreCurrentCalls']

            except (UnknownService, RpcTimeout):
                pass


    @api.model
    def notify_user(self, message, title=None, sticky=False, level='info'):
        self.ensure_one()
        self.env['bus.bus'].sendone(
                            'amanita_base_notify_{}'.format(self.env.user.id),
            {'message': message, 'title': title, 
             'level': level, 'sticky': sticky})

