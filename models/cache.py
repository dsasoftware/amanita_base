from odoo import models, fields, api, _
from datetime import datetime, timedelta

DEFAULT_EXPIRE_SECONDS = 3600 * 24 # One day


class Cache(models.Model):
    """
    Simple key/value storage with expiration feature.
    Duplicate keys are accepted but get returns the first key.
    """
    # TODO: ORM Cache

    _name = 'amanita_base.cache'
    _description = 'Cache Impementation'
    _log = False # TODO: Check that auto disables special fields

    key = fields.Char(required=True, index=True)
    family = fields.Char(required=True, default='/')
    value = fields.Char(required=True)
    expire = fields.Integer(required=True)
    created = fields.Datetime(required=True, index=True)

    @api.model
    def put(self, key, value, family='/', expire=DEFAULT_EXPIRE_SECONDS):
        self.sudo().create({
            'key': key,
            'family': family,
            'value': value,
            'expire': expire,
            'created': datetime.utcnow(),
        })
        self.do_expiration()
        return True
    
    @api.model
    def get(self, key, family='/'):
        self.do_expiration()
        res = self.sudo().search([('family', '=', family), ('key', '=', key)])
        if res:
            return res[0].value
        else:
            return False


    @api.model
    def do_expiration(self):
        now = datetime.utcnow()
        for rec in self.sudo().search([]):
            if rec.created + timedelta(seconds=rec.expire) < now:
                rec.unlink()
