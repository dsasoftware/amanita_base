import json
import logging
from odoo import fields, models, api, _
from .utils import remove_empty_lines

logger = logging.getLogger(__name__)


class Dialplan(models.Model):
    _name = 'amanita_base.dialplan'
    _rec_name = 'context'
    _description = "Dialplan"

    server = fields.Many2one(comodel_name='amanita_base.server', required=True)
    context = fields.Char(required=True)
    extension = fields.Many2one('amanita_base.extension')
    exten = fields.Char(
                related='extension.number', required=True, readonly=False)
    #extension = fields.Char(required=True)
    # extension_id = fields.One2many('amanita_base.extension', inverse_name='dialplan')
    note = fields.Text()
    lines = fields.One2many('amanita_base.dialplan_line', inverse_name='dialplan')
    is_custom = fields.Boolean(string="Custom code")
    custom_dialplan = fields.Text()
    dialplan = fields.Text(compute='_get_dialplan')

    _sql_constraints = [
        ('context_uniq', 'unique (context,server)', _('The context must be unique !')),
    ]


    @api.model
    def create(self, vals):
        rec = super(Dialplan, self).create(vals)
        extension = self.env['amanita_base.extension'].with_context(
                                {'no_build_conf': True}).create({
                                                'server': rec.server.id,
                                                'app': 'amanita_base',
                                                'model': 'dialplan',
                                                'number': vals['exten'],
                                                'obj_id': rec.id})
        rec.extension = extension.id
        self.build_conf()
        self.env['amanita_base.extension'].build_conf()
        return rec


    @api.multi
    def write(self, vals):
        res = super(Dialplan, self).write(vals)
        if res and not self.env.context.get('no_build_conf'):
            self.build_conf()
            self.env['amanita_base.extension'].build_conf()
        return res


    @api.multi
    def unlink(self):
        for rec in self:            
            rec.extension.unlink()            
        res = super(Dialplan, self).unlink()
        if res:
            self.build_conf()
            self.env['amanita_base.extension'].build_conf()
        return res


    @api.multi
    def render_extension(self):
        # Render extension from template
        self.ensure_one()
        return self.env['ir.qweb'].render('amanita_base.dialplan_extension',
                                   {'rec': self}).decode('latin-1')


    def compile_dialplan(self):
        self.ensure_one()
        if self.is_custom:
            return self.custom_dialplan or ''
        else:
            ret_lines = []
            prio = 1
            for line in self.lines:
                if not line.label:
                    ret_lines.append('exten => {},{},{}({})'.format(
                                self.exten, prio, line.app,
                                line.app_data if line.app_data else ''))
                else:
                    ret_lines.append('exten => {},{}({}),{}({})'.format(
                                self.exten, prio, line.label, line.app,
                                line.app_data if line.app_data else ''))
                prio += 1
            return '\n'.join(ret_lines)


    @api.model
    def build_conf(self):
        dialplans = self.env['amanita_base.dialplan'].search([])
        for server in dialplans.mapped('server'):
            conf_data = ''
            for dp in dialplans.filtered(lambda r: r.server == server):
                conf_data += '[{}]; {}\n'.format(dp.context, dp.note or '')
                conf_data += dp.compile_dialplan()
                conf_data += '\n\n'
            conf = self.env['amanita_base.conf'].get_or_create(
                                                server.id,
                                                'extensions_odoo_custom.conf')
            conf.content = '{}'.format(
                                remove_empty_lines(conf_data))
            conf.include_from('extensions.conf')

    @api.multi
    @api.onchange('lines')
    def _get_dialplan(self):
        for rec in self:
            rec.dialplan = rec.custom_dialplan if rec.is_custom else \
                                                    rec.compile_dialplan()


class DialplanLine(models.Model):
    _name = 'amanita_base.dialplan_line'
    _order = 'sequence'
    _description = "Dialplan line"

    name = fields.Char(compute='_get_name')
    dialplan = fields.Many2one('amanita_base.dialplan')
    exten = fields.Char()
    sequence = fields.Integer()
    app = fields.Char()
    app_data = fields.Char()
    label = fields.Char()


    @api.multi
    def _get_name(self):
        for rec in self:
            rec.name = '{}({})'.format(rec.app, rec.app_data or '')
