import string
import logging
from odoo import models, fields, api, _
from odoo.exceptions import ValidationError
from .utils import remove_empty_lines

logger = logging.getLogger(__name__)


class Extension(models.Model):
    _name = 'amanita_base.extension'
    _order = 'number'
    _description = "Extension"

    name = fields.Char(compute='_get_name')
    server = fields.Many2one(comodel_name='amanita_base.server', required=True,
                             ondelete='cascade')
    number = fields.Char(required=True)
                         #compute='_get_number', store=True,
                         #inverse='_set_number')
    app = fields.Char(required=True, readonly=True)
    model = fields.Char(required=True, readonly=True)
    app_model = fields.Char(compute='_get_app_model')
    obj_id = fields.Integer(required=True, readonly=True)
    record_calls = fields.Boolean()


    @api.model
    def create(self, vals):
        res = super(Extension, self).create(vals)
        if res:
            self.build_conf()
        return res


    @api.multi
    def write(self, vals):
        res = super(Extension, self).write(vals)
        if res:
            self.build_conf()
        return res


    @api.constrains('number')
    def _check_number(self):
        if not self.number:
            return
        # Check for existance
        count = self.env['amanita_base.extension'].search_count([
                                            ('server', '=', self.server.id),
                                            ('number', '=', self.number)])
        if count > 1:
            raise ValidationError(_('This extension number is already used!'))


    @api.multi
    def _get_app_model(self):
        for rec in self:
            rec.app_model = '{}.{}'.format(rec.app, rec.model)


    @api.multi
    def get_object(self):
        self.ensure_one()
        return self.env[self.app_model].browse(self.obj_id)


    @api.multi
    def _get_name(self):
        for rec in self:
            obj = self.env[rec.app_model].browse(rec.obj_id)
            object_name = getattr(obj, obj._rec_name, False)
            rec.name = '{} ({}) @ {}'.format(
                                    rec.number, object_name, rec.server.name)

    @api.model
    def build_conf(self):
        if self.env.context.get('no_build_conf'):
            logger.debug('Extension no_build_conf set, return.')
            return False
        extensions = self.env['amanita_base.extension'].search([])
        servers = extensions.mapped('server')
        for server in servers:
            conf = self.env['amanita_base.conf'].get_or_create(
                                                server.id,
                                                'extensions_odoo.conf')
            dialplan = '[odoo-extensions]\n'
            for ext in extensions.filtered(lambda r: r.server == server):
                dialplan += self.env['ir.qweb'].render(
                    'amanita_base.extension', {'rec': ext}).decode('latin-1')
                dialplan += ext.get_object().render_extension()
                dialplan += ';'
            conf.content = '{}'.format(remove_empty_lines(dialplan))
            conf.include_from('extensions.conf')
        return True


    @api.multi
    def open_extension(self):
        self.ensure_one()
        res = {
            'type': 'ir.actions.act_window',
            'res_model': self.app_model,
            'view_mode': 'form',
            'view_type': 'form',
            'target': 'current',
            'res_id': self.obj_id,
            # TODO: When SIP app is ready check how it works. May be keep context
            # in extension
        }
        return res
