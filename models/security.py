import logging
from odoo import models, fields, api, _

logger = logging.getLogger(__name__)

class AccessList(models.Model):
    _name = 'amanita_base.access_list'
    _description = 'Access Lists of IP/Nets'
    _order = 'address'

    server = fields.Many2one('amanita_base.server', required=True,
                             ondelete='cascade')
    name = fields.Char(compute='_get_name')
    address = fields.Char(required=True, index=True)
    netmask = fields.Char()
    address_type = fields.Selection([('ip', 'IP Address'), ('net', 'Network')],
                                    required=True, default='ip')
    access_type = fields.Selection([('allow', 'Allow'), ('deny', 'Deny')],
                                   required=True, default='deny')



    @api.multi
    def _get_name(self):
        for rec in self:
            rec.name = rec.address if rec.address_type == 'ip' else \
                '{}/{}'.format(rec.address, rec.netmask)


    @api.model
    def create(self, vals):
        # TODO: Handle CSV import
        res = super(AccessList, self).create(vals)
        self.update_rules()        
        return res


    @api.multi
    def write(self, vals):
        res = super(AccessList, self).write(vals)
        self.update_rules()
        return res


    @api.multi
    def unlink(self):
        res = super(AccessList, self).unlink()
        self.update_rules()
        return res


    @api.model
    def update_rules(self):
        entries = self.search([])
        for server in entries.mapped('server'):
            server_entries = entries.filtered(lambda r: r.server == server)
            with server.get_proxy('security') as proxy:
                rules = []
                for entry in server_entries:
                    rules.append({
                        'address': entry.address,
                        'netmask': entry.netmask,
                        'address_type': entry.address_type,
                        'access_type': entry.access_type,
                    })                
                proxy.update_access_rules(rules)


class Ban(models.Model):
    _name = 'amanita_base.access_ban'
    _description = 'Access Bans'
    _order = 'address'

    server = fields.Many2one('amanita_base.server', required=True,
                             ondelete='cascade')
    address = fields.Char(index=True, required=True)
    timeout = fields.Integer()
    packets = fields.Integer()
    bytes = fields.Integer()
    comment = fields.Char(index=True)


    @api.multi
    def read(self, fields=None, load='_classic_read'):
        ret = []
        # Get servers and read bans from each
        my_id = 1
        for server in self.env['amanita_base.server'].search([]):
            with server.get_proxy('security') as proxy:
                res_data = proxy.get_banned()
                for res in res_data:
                    ret.append({
                        'id': my_id,
                        'server': (server.id, server.name),
                        'comment': res['comment'],
                        'timeout': res['timeout'],
                        'display_name': 'amanita_base.access_ban,{}'.format(my_id),
                        'bytes': res['bytes'],
                        'packets': res['packets'],
                        'address': res['address'],
                    })
                    my_id += 1
        return ret
    

    @api.model
    def search_read(self, domain=None, fields=None, offset=0, limit=None, order=None):
        # TODO: search by domain
        return self.read()


    @api.multi
    def unlink(self):
        entries = {}
        # Populate entries with ip addresses
        for rec in self:
            entries.setdefault(rec.server, []).append(rec.address)
        res = super(Ban, self).unlink()
        # Now send to agent
        if res:
            for server in entries.keys():
                with server.get_proxy('security') as proxy:
                    proxy.remove_banned_addresses(entries[server])
        return res
