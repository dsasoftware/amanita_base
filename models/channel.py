from datetime import datetime
import json
import logging
from odoo import models, fields, api, _

logger = logging.getLogger(__name__)


class Channel(models.Model):
    _name = 'amanita_base.channel'
    _rec_name = 'channel'
    _description = 'Asterisk Channel'

    channel = fields.Char(index=True)
    uniqueid = fields.Char(size=150, index=True)
    linkedid = fields.Char(size=150, index=True)
    context = fields.Char(size=80)
    connected_line_num = fields.Char(size=80, string=_('Line number'))
    connected_line_name = fields.Char(size=256, string=_('Line name'))
    state = fields.Char(size=80)
    state_desc = fields.Char(size=256, string="State Description")
    exten = fields.Char(size=32)
    callerid_num = fields.Char(size=32)
    callerid_name = fields.Char(size=256)
    system_name = fields.Char(size=32)
    accountcode = fields.Char(size=80)
    priority = fields.Char(size=4)
    timestamp = fields.Char(size=20)
    app = fields.Char(size=32, string='Application')
    app_data = fields.Char(size=512, string='Application Data')
    duration = fields.Char(compute='_get_duration')
    language = fields.Char(size=2)
    event = fields.Char()
    privilege = fields.Char()
    content = fields.Char()

    @api.multi
    def _get_duration(self):
        for rec in self:
            rec.duration = str(datetime.now() - fields.Datetime.to_datetime(
                                            rec.create_date)).split('.')[0]

    @api.multi
    def refresh(self):
        # For UI to reload the form
        return True


    @api.model
    def new_channel(self, event, skip_check=False):
        values = event['headers']
        if not skip_check:
            channel = self.env['amanita_base.channel'].search(
                [('uniqueid', '=', values.get('Uniqueid'))])
            if channel:
                # Update channel state event came before newchannel event :-)
                return False      
        data = {
            'channel': values.pop('Channel', False),
            'uniqueid': values.pop('Uniqueid', False),
            'linkedid': values.pop('Linkedid', False),
            'context': values.pop('Context', False),
            'connected_line_num': values.pop('ConnectedLineNum', False),
            'connected_line_name': values.pop('ConnectedLineName', False),
            'state': values.pop('ChannelState', False),
            'state_desc': values.pop('ChannelStateDesc', False),
            'exten': values.pop('Exten', False),
            'callerid_num': values.pop('CallerIDNum', False),
            'callerid_name': values.pop('CallerIDName', False),
            'accountcode': values.pop('AccountCode', False),
            'priority': values.pop('Priority', False),
            'timestamp': values.pop('Timestamp', False),
            'system_name': values.pop('SystemName', False),
            'language': values.pop('Language', False),
            'privilege': values.pop('Privilege', False),
            'event': values.pop('Event', False),
            'content': values.pop('content', False),
        }
        if values:
            data.update(values)
        self.env['amanita_base.channel'].create(data)
        self.env['bus.bus'].sendone('amanita_base_channels',
                                    {'command': 'reload'})
        return True


    @api.model
    def update_channel_state(self, event):
        orig_values = event['headers']
        values = event['headers'].copy()
        uniqueid = values.pop('Uniqueid', False)        
        if not uniqueid:
            logger.info('No Uniqueid field in channel update state event: %s',
                        orig_values)
            return False
        channel = self.env['amanita_base.channel'].search(
                                                [('uniqueid', '=', uniqueid)])
        if not channel:
            # Newstate event came before Newchannel event, it's ok :-)
            return self.new_channel(event, skip_check=True)
        # We pop known fields so that we could add additional fields on event
        data = {
            'channel': values.pop('Channel', False),
            'linkedid': values.pop('Linkedid', False),
            'context': values.pop('Context', False),
            'connected_line_num': values.pop('ConnectedLineNum', False),
            'connected_line_name': values.pop('ConnectedLineName', False),
            'state': values.pop('ChannelState', False),
            'state_desc': values.pop('ChannelStateDesc', False),
            'exten': values.pop('Exten', False),
            'callerid_num': values.pop('CallerIDNum', False),
            'callerid_name': values.pop('CallerIDName', False),
            'accountcode': values.pop('AccountCode', False),
            'priority': values.pop('Priority', False),
            'app': values.pop('Application', False),
            'app_data': values.pop('AppData', False),
            'system_name': values.pop('SystemName', False),
            'language': values.pop('Language', False),
            'privilege': values.pop('Privilege', False),
            'event': values.pop('Event', False),
            'content': values.pop('content', False),
        }
        # Here it is. We can pass some custom fields to event and they are set.
        if values:
            data.update(values)
        # Update record and send notification
        channel.write(data)
        self.env['bus.bus'].sendone('amanita_base_channels',
                                {'command': 'reload'})
        return True


    @api.model
    def hangup_channel(self, event):
        values = event['headers']
        uniqueid = values.get('Uniqueid')
        channel = values.get('Channel')
        found = self.env['amanita_base.channel'].search([('uniqueid', '=', uniqueid)])
        if found:
            logger.debug('Found channel {}'.format(channel))
            # Check for recording
            found.unlink()
        else:
            logger.info('Channel {} not found for hangup.'.format(uniqueid))
        self.env['bus.bus'].sendone('amanita_base_channels',
                                    {'command': 'reload'})
        return True


