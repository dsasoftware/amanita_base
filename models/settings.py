import logging
import uuid
from odoo import api, fields, models, _, SUPERUSER_ID

logger = logging.getLogger(__name__)

INTER_PROTOCOLS = [('iax', 'IAX2'), ('sip', 'SIP')]


def get_nameko_config(obj):
    config = {
        'AMQP_URI': obj.env['res.config.settings'].get_amanita_param(
                                                                'amqp_uri'),
    }
    return config


AMANITA_BASE_PARAMS = [
    ('interconnection_protocol', 'iax'),
    ('device_registration_token', 'change-me'),
    ('recording_storage', 'filestore'),
    ('amqp_uri', 'pyamqp://guest:guest@rabbitmq'),
    ('console_uri', 'ws://127.0.0.1:8001/console-ws/'),
    ('rpc_timeout', '5'),
]


class AmanitaBaseSettings(models.TransientModel):
    _inherit = 'res.config.settings'    


    interconnection_protocol = fields.Selection(INTER_PROTOCOLS)
    device_registration_token = fields.Char()
    recording_storage = fields.Selection([('db', _('Database')),
                                          ('filestore', _('Files'))])
    amqp_uri = fields.Char()
    console_uri = fields.Char()
    rpc_timeout = fields.Char()


    @api.multi
    def set_values(self):
        super(AmanitaBaseSettings, self).set_values()
        if not (self.env.user.id == SUPERUSER_ID or
                self.env.user.has_group(
                                'amanita_base.group_amanita_base_admin')):
            # Do not save values!
            return
        for field_name in dict(AMANITA_BASE_PARAMS):
            value = getattr(self, field_name)
            self.env['ir.config_parameter'].sudo().set_param(
                'amanita_base.' + field_name, value)

    @api.model
    def get_values(self):
        res = super(AmanitaBaseSettings, self).get_values()
        if not (self.env.user.id == SUPERUSER_ID or
                self.env.user.has_group(
                                'amanita_base.group_amanita_base_admin')):
            # Do not return values!
            return res
        for field_name, default_value in AMANITA_BASE_PARAMS:
            res[field_name] = self.env[
                'ir.config_parameter'].get_param(
                    'amanita_base.' + field_name, default_value)
        return res


    @api.model
    def get_amanita_param(self, param):
        result = self.env['ir.config_parameter'].sudo().get_param(
                                                    'amanita_base.' + param)
        if not result:
            # Get default value
            result = dict(AMANITA_BASE_PARAMS).get(param)
        logger.debug(u'Param {} = {}'.format(param, result))
        return result


    @api.model
    def set_amanita_param(self, param, value):
        # set_ method are deprecated and all wrapped to set_values so we use _set_
        return self.env['ir.config_parameter'].sudo().set_param(
                'amanita_base.' + param, value)


    @api.model
    def _set_new_param(self, param, value):
        # It's called from data/settings.xml
        if not self.get_amanita_param(param):
            self.set_amanita_param(param, value)


    @api.multi
    def generate_asterisk_device_registration_token_button(self):
        self.generate_asterisk_device_registration_token()
        return {
            'type': 'ir.actions.client',
            'tag': 'reload'
        }

    @api.model
    def generate_asterisk_device_registration_token(self):
        self._set_asterisk_param('device_registration_token', '{}'.format(
                                                            uuid.uuid4().hex))

    @api.multi
    def sync_recording_storage(self):
        count = 0
        try:
            if self.recording_storage == 'db':
                search_field = 'recording_attachment'
            else:
                search_field = 'recording_data'
            while True:
                records = self.env['amanita_base.cdr'].sudo().search(
                                        [(search_field, '!=', False)],
                                        limit=100)
                if not records:
                    break
                logger.info('Sync %s records', len(records))
                for rec in records:
                    if self.recording_storage == 'db' and not rec.recording_data:
                        logger.info('Moving call id %s from filestore to db', rec.id)
                        rec.recording_data = rec.recording_attachment
                        rec.recording_attachment = False
                        count += 1
                    elif self.recording_storage == 'filestore' and not rec.recording_attachment:
                        logger.info('Moving call id %s from db to filestore', rec.id)
                        rec.recording_attachment = rec.recording_data
                        rec.recording_data = False
                        count += 1
                    self.env.cr.commit()
        except Exception:
            # Remove attachments
            logger.exception('Sync error:')
        finally:
            logger.info('Moved %s recordings', count)
            self.env['ir.attachment']._file_gc()

    @api.model
    def check_amanita_group(self, user_id):
        # Used from channels.js to enable bus poll on channels
        user = self.env['res.users'].browse(user_id)
        if user:
            if (user.has_group('amanita_base.group_amanita_base_user') or
                    user.has_group('amanita_base.group_amanita_base_user') or
                    user.has_group('amanita_base.group_amanita_base_user')):
                return True


