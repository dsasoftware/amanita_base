odoo.define("amanita_base.channels", function (require) {
    "use strict";
  
    var WebClient = require('web.WebClient');
    var ajax = require('web.ajax');
    var utils = require('mail.utils');
    var session = require('web.session');    
    var channel = 'amanita_base_channels';

    WebClient.include({
        start: function(){
            this._super()
            self = this
            ajax.rpc('/web/dataset/call_kw', {
                    "model": "res.config.settings",
                    "method": "check_amanita_group",
                    "args": [session.uid],
                    "kwargs": {},
            }).then(function (has_group) {
              if (has_group) {
                // Start polling
                self.call('bus_service', 'addChannel', channel);
                self.call('bus_service', 'onNotification', self, 
                      self.on_amanita_base_channels_notification)
                //console.log('Listening on ', channel)
              }
              else {
                //console.log('Not listening on channels events')
              }
            }
        )},

        on_amanita_base_channels_notification: function (notification) {
          for (var i = 0; i < notification.length; i++) {
             var ch = notification[i][0]
             var msg = notification[i][1]
             if (ch == channel) {
                 try {
                  this.handle_amanita_base_message(msg)
                }
                catch(err) {console.log(err)}
             }
           }
        },

        handle_amanita_base_message: function(msg) {
          //console.log(msg)
          if (typeof msg == 'string')
            var message = JSON.parse(msg)
          else
            var message = msg
          //console.log(message)
          var action = this.action_manager.getCurrentAction();
          if (!action) {
              //console.log('Action not loaded')
              return
          }
          var controller = this.action_manager.getCurrentController();
          if (!controller) {
              //console.log('Controller not loaded')
              return
          }
          if (controller.widget.modelName == "amanita_base.channel" && 
                controller.widget.viewType == 'list') {
                //console.log('Reloading')
              controller.widget.reload()
          }

        },
    })
})

