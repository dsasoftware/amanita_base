odoo.define('amanita_base.server_cli', function(require) {
    "use strict";

  var field_registry = require('web.field_registry');
  var basicFields = require('web.basic_fields');

  var ServerCli = basicFields.DebouncedField.extend({    
      template: 'ServerCli',
      events: {},

      _render: function() {
        this._super();
        var self = this;
        var button = document.createElement('button');
        button.setAttribute('class', 'btn btn-info bt-lg');
        button.innerHTML = 'Console';
        button.onclick = function() {
          window.open(self.value, 'Terminal', 
            'width=1024,height=900,top=10,left=10,menubar=no,toolbar=no,location=no,status=no,scrollbars=yes');
        }
        self.$el[0].appendChild(button);
      },

    });

    field_registry.add('server_cli', ServerCli);
});    